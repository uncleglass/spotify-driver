package pl.uncleglass;

import java.util.Scanner;

public class UserInterface {
    private static final  Scanner scanner = new Scanner(System.in);

    public static String read(String message) {
        write(message);
        return scanner.nextLine().trim();
    }

    public static void write(String message) {
        System.out.println(message);
    }

    public static void write(String message, String... args) {
        System.out.printf(message, args);
    }
}
