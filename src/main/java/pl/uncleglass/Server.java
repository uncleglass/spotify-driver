package pl.uncleglass;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;

public class Server {
    private final HttpServer httpServer;
    private final Object LOCK;

    public Server(Object LOCK) throws IOException {
        this.LOCK = LOCK;
        httpServer = getServer();
    }

    private HttpServer getServer() throws IOException {
        final HttpServer server;
        server = HttpServer.create();
        server.bind(new InetSocketAddress(8080), 0);
        server.createContext("/",
                httpExchange -> {
                    String response = "Authorization code not found. Try again.";
                    String query = httpExchange.getRequestURI().getQuery();
                    Map<String, String> parsedQuery = Parser.parseQuery(query);
                    if (parsedQuery.containsKey("code")) {
                        String code = parsedQuery.get("code");
                        Property.setCODE(code);
                        UserInterface.write("code received");
                        response = "Got the code. Return back to your program.";
                    }
                    httpExchange.sendResponseHeaders(200, response.length());
                    httpExchange.getResponseBody().write(response.getBytes());
                    httpExchange.getResponseBody().close();
                    synchronized (LOCK) {
                        LOCK.notify();
                    }
                }
        );
        return server;
    }

    public void start() {
        httpServer.start();
    }

    public void stop() {
        httpServer.stop(1);
    }
}
