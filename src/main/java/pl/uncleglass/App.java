package pl.uncleglass;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.URISyntaxException;

public class App {
    private boolean access = false;
    Client client = new Client();

    public void start() {
        try {
            authenticate();
            while (true) {

                String albumName = UserInterface.read("Pass album name: ");
                if ("exit".equals(albumName)) {
                    return;
                }
                String response = client.searchAlbum(albumName);
                UserInterface.write(response);

                Response response1 = new Gson().fromJson(response, Response.class);
                Albums albums = response1.getAlbums();
                String uri = albums.getItems().get(0).getUri();

                String devices = client.getDevices();
                Property.setDEVICES(new Gson().fromJson(devices, Devices.class));

                Device device = Property.DEVICES.getDevices().stream()
                        .filter(d -> "DEV-LAPTOP".equals(d.getName()))
                        .findFirst()
                        .orElseThrow();

                client.sendAlbum(device.getId(), uri);
            }

        } catch (IOException | InterruptedException | URISyntaxException e) {
            e.printStackTrace();
        }

    }

    private void authenticate() throws IOException, InterruptedException, URISyntaxException {
        AuthenticationService authenticationService = new AuthenticationService();
        access = authenticationService.authenticate();
        if (access) {
            UserInterface.write("authenticated");
        }
    }

}
