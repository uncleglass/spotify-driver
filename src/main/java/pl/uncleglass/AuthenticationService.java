package pl.uncleglass;

import com.google.gson.Gson;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class AuthenticationService {
    public static final Object LOCK = new Object();
    Desktop desk = Desktop.getDesktop();
    private Server server;

    public boolean authenticate() throws IOException, InterruptedException, URISyntaxException {
        startServer();
        displayLink();
        stopServer();
        String token = getToken();
        UserInterface.write(token);
        if (!token.isEmpty()) {
            Property.setTOKEN(new Gson().fromJson(token, Token.class));
            return true;
        }
        return false;
    }

    private void startServer() throws IOException {
        server = new Server(LOCK);
        server.start();
    }

    private void stopServer() {
        server.stop();
    }

    private void displayLink() throws InterruptedException, IOException, URISyntaxException {
        String uri = "https://accounts.spotify.com/authorize?" +
                "client_id=" + Property.CLIENT_ID +
                "&redirect_uri=" + Property.SERVER_URI + ":" + Property.SERVER_PORT +
                "&response_type=code" +
                "&scope=user-read-playback-state%20user-modify-playback-state";

        desk.browse(new URI(uri));
        synchronized (LOCK) {
            LOCK.wait();
            System.in.read(new byte[System.in.available()]);
        }
    }

    private String getToken() throws IOException, InterruptedException {
        UserInterface.write("making http request for access_token...");
        Client client = new Client();
        return client.getAccessToken();
    }
}
