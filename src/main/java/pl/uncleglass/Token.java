package pl.uncleglass;

public class Token {
    private String access_token;
    private String token_type;
    private int expires_in;
    private String refresh_token;

    @Override
    public String toString() {
        return token_type + " " + access_token;
    }
}
