package pl.uncleglass;

import java.util.HashMap;
import java.util.Map;

public class Parser {
    private Parser() {
    }

    public static Map<String, String> parseQuery(String query) {
        String[] queries;
        if (query == null) {
            queries = new String[0];
        } else {
            queries = query.split("&");
        }
        Map<String, String> arguments = new HashMap<>();
        for (String q : queries) {
            String[] kv = q.split("=");
            if (kv.length != 2) {
                throw new IllegalStateException("There is a problem: key is present but no value.");
            }
            arguments.put(kv[0], kv[1]);
        }
        return arguments;
    }
}
