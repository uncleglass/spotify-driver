package pl.uncleglass;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Client {
    private final HttpClient httpClient = HttpClient.newBuilder().build();

    public String getAccessToken() throws IOException, InterruptedException {
        String uri = "https://accounts.spotify.com/api/token";

        String query = "grant_type=authorization_code" +
                "&code=" + Property.CODE +
                "&redirect_uri=" + Property.SERVER_URI + ":" + Property.SERVER_PORT +
                "&client_id=" + Property.CLIENT_ID +
                "&client_secret=" + Property.CLIENT_SECRET +
                "&scope=user-read-playback-state user-modify-playback-state";

        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .uri(URI.create(uri))
                .POST(HttpRequest.BodyPublishers.ofString(query))
                .build();

        HttpResponse<String> response =
                httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }


    public String searchAlbum(String albumName) throws IOException, InterruptedException {
        String uri = Property.ROOT_ENDPOINT + "/v1/search?q="
                + albumName.replace(" ", "%20") +
                "&type=album";

        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .header("Authorization", Property.TOKEN.toString())
                .uri(URI.create(uri))
                .GET()
                .build();

        HttpResponse<String> response =
                httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public String getDevices() throws IOException, InterruptedException {
        String uri = Property.ROOT_ENDPOINT + "/v1/me/player/devices";

        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", Property.TOKEN.toString())
                .uri(URI.create(uri))
                .GET()
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body();
    }

    public void sendAlbum(String deviceId, String contextUri) throws IOException, InterruptedException {
        String uri = Property.ROOT_ENDPOINT + "/v1/me/player/play?device_id=" + deviceId;

        String body = "{\"context_uri\":\"" + contextUri + "\",\"offset\":{\"position\":0},\"position_ms\":0}";

        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", Property.TOKEN.toString())
                .uri(URI.create(uri))
                .PUT(HttpRequest.BodyPublishers.ofString(body))
                .build();

        httpClient.send(request, HttpResponse.BodyHandlers.ofString());
    }
}
