package pl.uncleglass;

public class Property {
    public static String ROOT_ENDPOINT;
    public static String SERVER_URI;
    public static String SERVER_PORT;
    public static String CLIENT_ID;
    public static String CLIENT_SECRET;
    public static String CODE;
    public static Token TOKEN;
    public static Devices DEVICES;

    static {
        ROOT_ENDPOINT = "https://api.spotify.com";
        SERVER_URI = "http://localhost";
        SERVER_PORT = "8080";
        CLIENT_ID = System.getenv("CLIENT_ID");
        CLIENT_SECRET = System.getenv("CLIENT_SECRET");
    }


    public static void setCODE(String code) {
        CODE = code;
    }

    public static void setTOKEN(Token token) {
        Property.TOKEN = token;
    }

    public static void setDEVICES(Devices devices) {
        Property.DEVICES = devices;
    }
}
