package pl.uncleglass;

public class Device {
    private String id;
    private boolean is_active;
    private boolean is_private_session;
    private boolean is_restricted;
    private String name;
    private String type;
    private int volume_percent;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}